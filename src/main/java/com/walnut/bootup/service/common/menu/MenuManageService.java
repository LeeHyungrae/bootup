package com.walnut.bootup.service.common.menu;

import com.walnut.bootup.domain.common.menu.MenuManage;

import java.util.List;

public interface MenuManageService {
    List<MenuManage> selectMenuManageList();

    MenuManage findByMenuNo(Long menuNo);
}
