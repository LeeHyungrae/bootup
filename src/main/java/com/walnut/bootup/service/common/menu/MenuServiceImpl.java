package com.walnut.bootup.service.common.menu;

import com.walnut.bootup.domain.common.menu.MainMenu;
import com.walnut.bootup.domain.login.LoginVO;
import com.walnut.bootup.mapper.MainMenuMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Log4j2
@Service
public class MenuServiceImpl implements MenuService {
    @Resource
    private MainMenuMapper mainMenuMapper;

    @Override
    @Cacheable(value = "selectMainMenuHead")
    public List<MainMenu> selectMainMenuHead(LoginVO loginVO) throws Exception {
        log.debug("(Ehache) MenuServiceImpl - selectMainMenuHead : Reload Testing");
        return this.mainMenuMapper.selectMainMenuHead(loginVO);
    }

    @Override
    @Cacheable(value = "selectMainMenuHeadSubList")
    public List<MainMenu> selectMainMenuHeadSubList(LoginVO loginVO) throws Exception {
        log.debug("(Ehache) MenuServiceImpl - selectMainMenuHeadSubList : Reload Testing");
        return this.mainMenuMapper.selectMainMenuHeadSubList(loginVO);
    }
}
