package com.walnut.bootup.service.common.log;

import com.walnut.bootup.domain.common.log.LoginLog;

public interface LoginLogService {
    int insertLoginLog(LoginLog loginLog) throws Exception;
}
