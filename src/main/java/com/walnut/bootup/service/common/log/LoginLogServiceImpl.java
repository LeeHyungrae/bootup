package com.walnut.bootup.service.common.log;

import com.walnut.bootup.domain.common.log.LoginLog;
import com.walnut.bootup.mapper.LoginLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginLogServiceImpl implements LoginLogService {

    @Autowired
    private LoginLogMapper loginLogMapper;

    @Override
    public int insertLoginLog(LoginLog loginLog) throws Exception {
        return this.loginLogMapper.insertLoginLog(loginLog);
    }

}
