package com.walnut.bootup.service.common.menu;

import com.walnut.bootup.domain.common.menu.MenuManage;
import com.walnut.bootup.repository.MenuManageRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class MenuManageServiceImpl implements MenuManageService {

    @Resource
    private MenuManageRepository menuManageRepository;

    @Override
    public List<MenuManage> selectMenuManageList() {
        return this.menuManageRepository.findAll();
    }

    @Override
    public MenuManage findByMenuNo(Long menuNo) {
        Optional<MenuManage> menuManage = this.menuManageRepository.findById(menuNo);
        return menuManage.orElseGet(MenuManage::new);
    }
}
