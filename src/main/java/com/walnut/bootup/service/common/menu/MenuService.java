package com.walnut.bootup.service.common.menu;

import com.walnut.bootup.domain.common.menu.MainMenu;
import com.walnut.bootup.domain.login.LoginVO;

import java.util.List;

public interface MenuService {
    List<MainMenu> selectMainMenuHead(LoginVO loginVO) throws Exception;

    List<MainMenu> selectMainMenuHeadSubList(LoginVO loginVO) throws Exception;
}
