package com.walnut.bootup.mapper;

import com.walnut.bootup.domain.common.menu.MainMenu;
import com.walnut.bootup.domain.login.LoginVO;

import java.util.List;

public interface MainMenuMapper {
    List<MainMenu> selectMainMenuHead(LoginVO loginVO) throws Exception;

    List<MainMenu> selectMainMenuHeadSubList(LoginVO loginVO) throws Exception;
}
