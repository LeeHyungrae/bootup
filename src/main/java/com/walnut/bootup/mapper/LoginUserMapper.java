package com.walnut.bootup.mapper;


import com.walnut.bootup.domain.login.LoginVO;

public interface LoginUserMapper {
    LoginVO selectLoginUser(String username) throws Exception;
}
