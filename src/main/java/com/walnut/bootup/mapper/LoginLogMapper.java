package com.walnut.bootup.mapper;

import com.walnut.bootup.domain.common.log.LoginLog;

public interface LoginLogMapper {
    int insertLoginLog(LoginLog loginLog) throws Exception;
}
