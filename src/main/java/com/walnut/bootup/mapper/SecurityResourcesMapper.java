package com.walnut.bootup.mapper;

import com.walnut.bootup.config.security.SecurityResources;
import com.walnut.bootup.config.security.SecurityRole;

import java.util.List;

public interface SecurityResourcesMapper {
    List<SecurityResources> selectRolePattrnList() throws Exception;

    List<SecurityRole> selectAuthorCodeList() throws Exception;
}
