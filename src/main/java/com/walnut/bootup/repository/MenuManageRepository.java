package com.walnut.bootup.repository;

import com.walnut.bootup.domain.common.menu.MenuManage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MenuManageRepository extends JpaRepository<MenuManage, Long> {

    Optional<MenuManage> findById(Long menuNo);

}
