package com.walnut.bootup;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@EnableCaching
@MapperScan(basePackages = "com.walnut.bootup.mapper")
@SpringBootApplication
public class BootupApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootupApplication.class, args);
    }
}
