package com.walnut.bootup.domain.login;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class LoginVO implements Serializable {
    /** 로그인 아이디 */
    private String userId;
    /** 로그인 비밀번호 */
    private String userPw;
    /** 계정 활성화 여부 */
    private Boolean enabled;
    /** 이름 */
    private String userNm;
    /** 사용자구분 */
    private String userSe;
    /** 사용자우편번호 */
    private String userZip;
    /** 사용자주소 */
    private String userAdres;
    /** 이메일주소 */
    private String userEmail;
    /** 조직(부서)ID */
    private String orgnztId;
    /** 조직(부서)명 */
    private String orgnztNm;
    /** 필수아이디 */
    private String esntlId;
    /** 권한목록 */
    private List<LoginAuth> authList;
}
