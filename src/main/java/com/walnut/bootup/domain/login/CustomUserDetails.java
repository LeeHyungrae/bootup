package com.walnut.bootup.domain.login;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomUserDetails extends User {
    private static final long serialVersionUID = 2517403097977414969L;

    private Object userInfo;

    public CustomUserDetails(String username, String password, boolean enabled,
                             boolean accountNonExpired, boolean credentialsNonExpired,
                             boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
                             Object userInfo) throws IllegalArgumentException {

        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.userInfo = userInfo;
    }

    public CustomUserDetails(String username, String password, boolean enabled, Collection<? extends GrantedAuthority> authorities, Object userInfo) throws IllegalArgumentException {
        this(username, password, enabled, true, true, true, authorities, userInfo);
    }

    public Object getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(Object userInfo) {
        this.userInfo = userInfo;
    }
}
