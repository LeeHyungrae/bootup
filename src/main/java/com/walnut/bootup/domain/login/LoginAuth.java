package com.walnut.bootup.domain.login;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginAuth implements Serializable {
    /** 보안설정대상 아이디 */
    private String userTargetId;
    /** 보유 권한 */
    private String authority;
}
