package com.walnut.bootup.domain.common.menu;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "NEWLETTNMENUINFO")
public class MenuManage {
    /** 메뉴정보 */
    /**
     * 메뉴번호
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long menuNo;
    /**
     * 메뉴순서
     */
    private int menuOrdr;
    /**
     * 메뉴명
     */
    private String menuNm;
    /**
     * 상위메뉴번호
     */
    private int upperMenuId;
    /**
     * 메뉴설명
     */
    private String menuDc;
    /**
     * 관련이미지경로
     */
    private String relateImagePath;
    /**
     * 관련이미지명
     */
    private String relateImageNm;
    /**
     * 프로그램파일명
     */
    private String progrmFileNm;

    /** 사이트맵 */
    /**
     * 생성자ID
     **/
    private String creatPersonId;

    /** 권한정보설정 */
    /**
     * 권한코드
     */
    private String authorCode;
}
