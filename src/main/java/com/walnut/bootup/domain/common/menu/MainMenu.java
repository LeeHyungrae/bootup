package com.walnut.bootup.domain.common.menu;

import lombok.Data;

import java.io.Serializable;

@Data
public class MainMenu implements Serializable {
    /** 메뉴번호 */
    private int menuNo;
    /** 메뉴순서 */
    private int menuOrdr;
    /** 메뉴명 */
    private String menuNm;
    /** 상위메뉴번호 */
    private int upperMenuId;
    /** 메뉴설명 */
    private String menuDc;
    /** 관련이미지경로 */
    private String relateImagePath;
    /** 관련이미지명 */
    private String relateImageNm;
    /** 프로그램파일명 */
    private String progrmFileNm;
    /** 프로그램 URL */
    private String chkURL;
}
