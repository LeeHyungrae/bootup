package com.walnut.bootup.domain.common.log;

import lombok.Data;

@Data
public class LoginLog {
	/** 로그ID */
	private String logId;
	/** 사용자ID */
	private String conectId;
	/** 접속IP */
	private String conectIp;
	/** 접속방식 */
	private String conectMthd;
	/** 에러발생여부 */
	private String errorOccrrAt;
	/** 에러코드 */
	private String errorCode;
	/** 생성일시 */
	private String creatDt;
}
