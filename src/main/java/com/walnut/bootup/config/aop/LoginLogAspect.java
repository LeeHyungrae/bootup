package com.walnut.bootup.config.aop;

import com.walnut.bootup.domain.common.log.LoginLog;
import com.walnut.bootup.domain.login.CustomUserDetails;
import com.walnut.bootup.domain.login.LoginVO;
import com.walnut.bootup.service.common.log.LoginLogService;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Log4j2
@Aspect
@Component
public class LoginLogAspect {
    @Autowired
    private LoginLogService loginLogService;

    // TODO: 로그인정책 적용(로그인 ip 제한) 관련 로그인/로그아웃 로그 작업
    @AfterReturning("execution(* com.walnut.bootup.config.handler.CustomLoginSuccessHandler*.*(..))")
    public void loginLogAdvice(JoinPoint jp) throws Throwable {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomUserDetails userDetails = (CustomUserDetails) principal;
        LoginVO user = (LoginVO) userDetails.getUserInfo();

        LoginLog loginLog = new LoginLog();
        loginLog.setConectId(user.getUserId());
//        loginLog.setConnectIp(request.getRemoteAddr());
//        loginLog.setErrorOccrrAt("N");
//        loginLog.setErrorCode("999");
        loginLog.setConectMthd("L"); // L 로그인 O 로그아웃

        this.loginLogService.insertLoginLog(loginLog);
    }

//    @AfterReturning("execution(* com.walnut.bootup.config.handler.CustomLoginSuccessHandler*.*(..))")
//    public void logoutLogAdvice(JoinPoint jp) throws Throwable {
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        CustomUserDetails userDetails = (CustomUserDetails) principal;
//        LoginVO user = (LoginVO) userDetails.getUserInfo();
//
//        LoginLog loginLog = new LoginLog();
//        loginLog.setConectId(user.getUserId());
//        loginLog.setErrorOccrrAt("N");
//        loginLog.setErrorCode("999");
//        loginLog.setConectMthd("O"); // L 로그인 O 로그아웃
//
//        this.loginLogService.insertLoginLog(loginLog);
//    }
}
