package com.walnut.bootup.config.handler;

import com.walnut.bootup.domain.login.CustomUserDetails;
import com.walnut.bootup.domain.login.LoginVO;
import com.walnut.bootup.mapper.LoginUserMapper;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.stream.Collectors;


@Log4j2
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private LoginUserMapper loginUserMapper;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("CustomUserDetailsService - loadUserByUsername");
        LoginVO userInfo = loginUserMapper.selectLoginUser(username);
        return userInfo == null ? null : new CustomUserDetails(userInfo.getUserId()
                , userInfo.getUserPw()
                , userInfo.getEnabled()
                , userInfo.getAuthList().stream().map(loginAuth -> new SimpleGrantedAuthority(loginAuth.getAuthority())).collect(Collectors.toList())
                , userInfo);
    }
}
