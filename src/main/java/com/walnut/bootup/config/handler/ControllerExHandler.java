package com.walnut.bootup.config.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Log4j2
@ControllerAdvice
public class ControllerExHandler {

    // TODO: loginLogMapper or 비즈니스 로직 테스트
    @ExceptionHandler
    public void handleControllerException(Exception exception) {
        log.info("ControllerExHandler - handleControllerException");
        log.error(exception.getMessage(), exception.getCause());
    }
}
