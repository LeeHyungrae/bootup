package com.walnut.bootup.config.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Log4j2
public class CustomLoginFailureHandler implements AuthenticationFailureHandler {

    @Resource
    private MessageSource messageSource;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        log.info("CustomLoginFailureHandler - onAuthenticationFailure");

        if (e instanceof BadCredentialsException || e instanceof  InternalAuthenticationServiceException     || e instanceof LockedException    ||
            e instanceof AccountExpiredException || e instanceof  AuthenticationCredentialsNotFoundException || e instanceof DisabledException  ||
            e instanceof CredentialsExpiredException) {
            request.setAttribute("loginFailMsg", messageSource.getMessage("login.fail", null, Locale.KOREAN));
        }

        request.getRequestDispatcher("/login").forward(request, response);
    }
}
