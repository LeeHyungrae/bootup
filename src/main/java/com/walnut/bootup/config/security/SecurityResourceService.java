package com.walnut.bootup.config.security;

import com.walnut.bootup.mapper.SecurityResourcesMapper;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class SecurityResourceService {

    private SecurityResourcesMapper securityResourcesMapper;

    public SecurityResourceService(SecurityResourcesMapper securityResourcesMapper) {
        this.securityResourcesMapper = securityResourcesMapper;
    }

    public LinkedHashMap<RequestMatcher, List<ConfigAttribute>> getResourceList() throws Exception {
        // {"result" : {"url" : ["ROLE_ADMIN", "ROLE_USER" ...]}}
        LinkedHashMap<RequestMatcher, List<ConfigAttribute>> result = new LinkedHashMap<>();
        List<SecurityResources> resourcesList = this.securityResourcesMapper.selectRolePattrnList();
        List<SecurityRole> roleList = this.securityResourcesMapper.selectAuthorCodeList();

        resourcesList.forEach(resource -> {
            List<ConfigAttribute> configAttributeList = new ArrayList<>();
            String urlRoleCode = resource.getRoleCode();
            roleList.forEach(role -> {
                if (role.getRoleCode().equals(urlRoleCode)) {
                    configAttributeList.add(new SecurityConfig(role.getAuthorCode()));
                }
            });
            result.put(new AntPathRequestMatcher(resource.getRolePttrn()), configAttributeList);
        });
        return result;
    }

}
