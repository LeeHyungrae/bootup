package com.walnut.bootup.config.security;

import lombok.Data;

@Data
public class SecurityResources {
    private String roleCode;
    private String rolePttrn;
}