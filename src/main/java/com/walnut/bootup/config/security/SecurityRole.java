package com.walnut.bootup.config.security;

import lombok.Data;

@Data
public class SecurityRole {
    private String roleCode;
    private String authorCode;
}
