package com.walnut.bootup.controller.main;

import com.walnut.bootup.config.security.SecurityResources;
import com.walnut.bootup.domain.login.CustomUserDetails;
import com.walnut.bootup.domain.login.LoginVO;
import com.walnut.bootup.service.common.menu.MenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Log4j2
@RequiredArgsConstructor
@Controller
public class MainController {

    @Resource
    private MenuService menuService;

    @RequestMapping("/main")
    public String actionMain(ModelMap model)  throws Exception {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomUserDetails userDetails = (CustomUserDetails) principal;
        LoginVO currentUser = (LoginVO) userDetails.getUserInfo();

        model.addAttribute("mainMenuHead", this.menuService.selectMainMenuHead(currentUser));
        model.addAttribute("mainMenuSub", this.menuService.selectMainMenuHeadSubList(currentUser));

        if (currentUser.getUserSe().equals("ADM")) {
            return "/common/main/admin";
        }
        return "/common/main/user";
    }

    @RequestMapping("/main/user")
    public String userMain()  throws Exception {
        return "/common/main/user";
    }

    @RequestMapping("/main/admin")
    public String adminMain()  throws Exception {
        return "/common/main/admin";
    }
}
