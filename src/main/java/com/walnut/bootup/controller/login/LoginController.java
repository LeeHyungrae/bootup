package com.walnut.bootup.controller.login;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Log4j2
@RequiredArgsConstructor
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String loginUserForm(String error, Model model) {
        return "/login/user";
    }

    @RequestMapping("/accessDuplicate")
    public String accessDuplicate() {
        log.info("loginController - accessDuplicate");
        return "/common/accessDuplicate";
    }

    @RequestMapping("/accessDenied")
    public String accessDenied() {
        log.info("loginController - accessDenied");
        return "/common/accessDenied";
    }
}
