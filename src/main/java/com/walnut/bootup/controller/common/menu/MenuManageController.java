package com.walnut.bootup.controller.common.menu;

import com.walnut.bootup.domain.common.menu.MenuManage;
import com.walnut.bootup.service.common.menu.MenuManageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Log4j2
@RequiredArgsConstructor
@Controller
public class MenuManageController {

    @Resource
    private MenuManageService menuManageService;

    @RequestMapping("/common/menu/manage")
    public String list(ModelMap modelMap) throws Exception {
        modelMap.addAttribute("manageList", this.menuManageService.selectMenuManageList());
        return "/common/menu/manageList";
    }
}
