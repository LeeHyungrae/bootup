package com.walnut.bootup.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Log4j2
@RequiredArgsConstructor
@Controller
public class CommonController {

    @RequestMapping("/")
    public String index() throws Exception {
        return "/index";
    }
}
