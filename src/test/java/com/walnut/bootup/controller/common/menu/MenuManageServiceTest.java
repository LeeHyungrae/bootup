package com.walnut.bootup.controller.common.menu;


import com.walnut.bootup.repository.MenuManageRepository;
import com.walnut.bootup.service.common.menu.MenuManageService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
@Log4j2
public class MenuManageServiceTest {

    @Autowired
    MenuManageService menuManageService;
    @Autowired
    MenuManageRepository menuManageRepository;

    @Test
    void 목록불러오기() {
        log.info("menuManage 목록 불러오기");
        this.menuManageRepository.findAll().forEach(System.out::println);
    }

    @Test
    void 메뉴NO불러오기() {
        log.info("menuManage 메뉴No 불러오기");
        System.out.println(this.menuManageRepository.findById(1010000L));
    }

}
